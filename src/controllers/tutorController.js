const { user, student, child } = require("../models");

class Tutor {
  //get porfile tutor
  static getDataTutor = (req, res) => {
    user
      .findOne({ where: { id: req.user.id } })
      .then((response) => {
        console.log(response);
        res.render("profile", response);
      })
      .catch((e) => {
        res.json({
          status: 422,
          message: "Error get Tutor",
        });
      });
  };

  // update data pribadi tutor
  static update = (req, res) => {
    const tutorDetail = req.params.id;
    const { jeniskelamin, tanggallahir, provinsi, gaji, deskripsi, phonenumber } = req.body;
    user
      .update(
        {
          jeniskelamin,
          tanggallahir,
          provinsi,
          gaji,
          deskripsi,
          phonenumber,
        },
        {
          where: { id: tutorDetail },
        }
      )
      .then((pembimbing) => {
        res.json({
          status: 201,
          data: pembimbing,
        });
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "gagal update data diri",
        });
      });
  };

  // tutor dashboard
  static getTutorDashboard = (req, res) => {
    student
      .findAll(
        {
          include: [
            {
              model: user,
              model: child,
            },
          ],
        },
        { where: { userId: req.user.id } }
      )
      .then((resp) => {
        console.log(resp);
        res.render("dashboardTutor");
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "err",
        });
      });
  };

  // tutor penawaran
  static getTutorOffer = (req, res) => {
    res.render("tutor_penawaran");
  };
}

module.exports = Tutor;
