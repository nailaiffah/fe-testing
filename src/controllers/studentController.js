const { child, student, service, tracking, user, subject } = require("../models");

class studentcont {
  static dashboardTutor = (req, res) => {
    student
      .findAll({
        include: [
          {
            model: child,
            service,
          },
        ],
      })
      .then((student) => {
        res.render("dashboardTutor.ejs", {
          data: student,
          message: "This is the data",
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };

  static studentDetail = async (req, res) => {
    const id = req.params.id;
    console.log("thisss id " + id);
    student
      .findAll(
        {
          include: [
            {
              model: child,
              include: [
                {
                  model: user,
                },
              ],
            },
            {
              model: service,
            },
          ],
        },
        {
          where: {
            id: id,
          },
        }
      )
      .then((student) => {
        res.render("detailMurid.ejs", {
          data: student,

          message: "This is the data",
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };
  static getLaporan = async (req, res) => {
    const id = req.params.id;
    tracking
      .findAll(
        { where: { studentId: id } },
        {
          include: [
            {
              model: student,
              include: [
                {
                  model: child,
                },
              ],
            },
            { model: service },
          ],
        }
      )
      .then((tracking) => {
        res.render("listLaporan.ejs", {
          data: tracking,
          message: "This is the data",
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };
  static createLaporan = (req, res) => {
    const laporan = {
      serviceId: req.body.serviceId,
      childId: req.body.childId,
      createdAt: req.body.createdAt,
      description: req.body.description,
      achievement: req.body.achievement,
      behavior: req.body.behavior,
    };
    tracking.create(laporan).then((tracking) => {
      res.render("tambahLaporan.ejs", {
        data: tracking,
      });
    });
  };
  static getaddLaporan = (req, res) => {
    res.render("tambahLaporan.ejs");
  };
  static create = (req, res) => {
    const { serviceId, childId } = req.body;
    models.student
      .create({ serviceId, childId })
      .then((murid) => {
        res.json({
          status: 200,
          message: "Create Succes !",
          data: murid,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };

  static update = (req, res) => {
    const studentsid = req.params.id;
    const { serviceId, childId } = req.body;
    models.student
      .update(
        {
          serviceId,
          childId,
        },
        {
          where: { id: studentsid },
        }
      )
      .then(() => {
        res.json({
          status: 200,
          data: { serviceId, childId },
          message: "Updated Succes !",
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };
  static index = (req, res) => {};

  static delete = (req, res) => {
    const studentsId = req.params.id;
    models.student
      .destroy({
        where: {
          id: studentsId,
        },
      })
      .then(() => {
        res.json({
          status: 200,
          message: "Delete Succes !",
        });
      });
  };

  // Penawaran terakhir
  static getOfferHistory = (req, res) => {
    services
      .findAll(
        {
          include: [
            {
              model: subject,
              user,
            },
          ],
        },
        { where: { userId: req.user.id } }
      )
      .then((response) => {
        res.render("tutor_penawaran", response);
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };

  // search penawaran terakhir by
  static searchOffer = (req, res) => {
    const subjectName = req.body.subjectName;
    services
      .findAll(
        {
          include: [
            {
              model: user,
              subject,
              where: {
                name: subjectName,
              },
            },
          ],
        },
        { where: { userId: req.user.id } }
      )
      .then((response) => {
        res.render("tutor_penawaran", response);
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };

  // Detail penawaran
  static offerDetail = (req, res) => {
    const temp = req.params.id;
    const temp2 = "PENDING";
    services
      .findOne(
        {
          include: [
            {
              model: subject,
              user,
              include: [
                {
                  model: child,
                },
              ],
            },
          ],
        },
        { where: { id: temp }, where: { status: temp2 } }
      )
      .then((response) => {
        res.render("tutor_menunggu", response);
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };
}

module.exports = studentcont;
