const router = require("express").Router();
const learning = require("../controllers/learning_material");

router.get("/", learning.index);
router.post("/create", learning.create);
router.patch("/:id", learning.update);
router.delete("/:id", learning.delete);

module.exports = router;
