"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class child extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      child.belongsTo(models.user, { foreignKey: "userId" });
      child.hasMany(models.student);
    }
  }
  child.init(
    {
      name: DataTypes.STRING,
      kelas: DataTypes.STRING,
      age: DataTypes.INTEGER,
      userId: DataTypes.INTEGER,
      birthday: DataTypes.DATE,
      deskripsi : DataTypes.STRING
    },
    {
      sequelize,
      modelName: "child",
    }
  );
  return child;
};
